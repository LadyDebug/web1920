﻿let editionMode = false;
let editedElement;


function createPost(post) {
    let res = document.querySelector("#text");
    let color = document.getElementById("colorEditor").value;
    let font = document.getElementById("select").value;
    let img = post.ImageUri;
    if (img == "")
        img = "Images/noImage.jpg";

    let temp = `
<div id="text" class="ArticleText">
    <h1 style="color:${color};font-family:${font}">${post.header} </h1>
    <img class="ArticleImg" src="${img}">
    <span class="text">
       ${post.content}    
    </span>
<button onclick="onBtnEdit(this)">Edit</button>
</div>
`
    res.insertAdjacentHTML("beforebegin", temp);

    document.querySelector(".sidenav").style.left = "-320px";

    document.getElementById("titleEditor").value = "";
    document.getElementById("ImgUri").value = "";
    document.getElementById("editorId").innerHTML = "";
    document.getElementById("colorEditor").value = "";
}

function onBtnOk() {
    if (editionMode == true) {
        editPost();
        editionMode = false;
        document.getElementsByTagName("h3")[0].innerText = "Create new post";
    }
    else {
    
        let newPost = {
            header: document.getElementById("titleEditor").value,
            ImageUri: document.getElementById("ImgUri").value,
            content: document.getElementById("editorId").innerHTML
        }
        createPost(newPost);
        event.preventDefault();
    }
}

function OpenNav() {
    document.querySelector(".sidenav").style.left = "0";
}
function CloseNav() {
    document.querySelector(".sidenav").style.left = "-320px";
    if (editionMode == true) {
        document.getElementById("titleEditor").value = "";
        document.getElementById("ImgUri").value = "";
        document.getElementById("editorId").innerHTML = "";
        document.getElementById("colorEditor").value = "";
        document.getElementById("select").value = "";
        document.getElementsByTagName("h3")[0].innerText = "Create new post";
        editionMode = false;
    }
}

function Command(cmd, param = '' ) {
    
    document.execCommand(cmd, false, param);
}

function getLink() {
    let link = prompt("Please enter URL:", "https://www.facebook.com/");
    if (link == null || link == "") {
        link = "https://www.facebook.com/";
    }
    return link;
}
    



function langChange() {
    event.preventDefault();

    let APIkey = "trnsl.1.1.20181001T045221Z.bc7301da69ddbad5.89c23df283bf7d38569b4eb09c3efabb86bd6870";
    let lang = document.querySelector(".langSelect").value;
    let headers = document.getElementsByTagName("h1");

    for (const itm of headers) {
        let textFrom = itm.innerText;
        let httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let data = JSON.parse(this.response);
                itm.innerText = data.text[0];
            }
        };
        let str = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + APIkey + "&text=" + textFrom + "&lang=" + lang;
        httpRequest.open('GET', str);
        httpRequest.send();
    }

    ////
    let ps = document.getElementsByClassName("text");

    for (const itm of ps) {
        let textFrom = itm.innerHTML;
        textFrom = encodeURIComponent(textFrom);
        let httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                let data = JSON.parse(this.response);
                itm.innerHTML = decodeURIComponent(data.text[0]);
            }
        };
        let str = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=" + APIkey + "&text=" + textFrom + "&lang=" + lang+"&format=html";
        httpRequest.open('GET', str);
        httpRequest.send();
    }

}

function rgbToHex(a) {
    a = a.replace(/[^\d,]/g, "").split(",");
    return "#" + ((1 << 24) + (+a[0] << 16) + (+a[1] << 8) + +a[2]).toString(16).slice(1)
}

function onBtnEdit(el) {
    editionMode = true;
    editedElement = el;

    let textFrom = el.parentElement;
    let title = textFrom.getElementsByTagName("h1")[0];
    let titleText = title.innerText;
    let titleColor = title.style.color;
    let titleFont = title.style.fontFamily;
    let context = textFrom.getElementsByClassName("text")[0].innerHTML;
    let imgUri = textFrom.getElementsByClassName("ArticleImg")[0].currentSrc;

    OpenNav();
    document.getElementsByTagName("h3")[0].innerText = "Edit post";

    document.getElementById("titleEditor").value = titleText;
    document.getElementById("colorEditor").value = rgbToHex(titleColor);
    document.getElementById("select").value = titleFont;
    document.getElementById("ImgUri").value = imgUri;
    document.getElementById("editorId").innerHTML = context;
}

function editPost() {
    let old = editedElement.parentElement;
    let title = old.getElementsByTagName("h1")[0];
    title.innerText = document.getElementById("titleEditor").value;
    title.style.color = document.getElementById("colorEditor").value;
    title.style.fontFamily = document.getElementById("select").value;
    old.getElementsByClassName("ArticleImg")[0].currentSrc = document.getElementById("ImgUri").value;
    old.getElementsByClassName("text")[0].innerHTML = document.getElementById("editorId").innerHTML;

    CloseNav();

    document.querySelector(".sidenav").style.left = "-320px";

    
}